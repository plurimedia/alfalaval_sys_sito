# Alfa Laval Sito Size your system - Divisione Marina

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve 
npm run start (x lanciarlo con il --fix lint)
```

### Esecuzione da locale
Da locale bisogna lanciare prima il server (alfalaval_sys_backend) altrimenti non può prendere i dati.
Per lanciare il client del backend in parallelo a questo client bisogna, in ordine, eseguire prima il backend e poi il sito.

### Heroku deploy
heroku git:remote -a alfalaval-sys-sito
git push heroku master
