const path = require('path');

module.exports = {
  devServer: {
    proxy: 'http://localhost:3001'
  },
  publicPath: './',
  pwa: {
    name: 'Alfalaval',
    themeColor: '#223A87',
    msTileColor: '#000000',
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      // swSrc is required in InjectManifest mode.
      swSrc: path.join(process.cwd(), 'src', 'service-worker.js'),
      swDest: path.join(process.cwd(), 'dist', 'service-worker.js'),
      globDirectory: path.join(process.cwd(), 'dist')
      // ...other Workbox options...
    },
    iconPaths: {
      favicon32: 'img/icons/favicon-32x32.png',
      favicon16: 'img/icons/favicon-16x16.png',
      appleTouchIcon: 'img/icons/apple-touch-icon-152x152.png?ver=1.1',
      maskIcon: 'img/icons/safari-pinned-tab.svg',
      msTileImage: 'img/icons/msapplication-icon-144x144.png'
    },
    manifestOptions: {
      icons: [
        {
          src: 'img/favicons/android-icon-36x36.png',
          sizes: '36x36',
          type: 'image/png',
          density: '0.75'
        },
        {
          src: 'img/favicons/android-icon-48x48.png',
          sizes: '48x48',
          type: 'image/png',
          density: '1.0'
        },
        {
          src: 'img/favicons/android-icon-72x72.png',
          sizes: '72x72',
          type: 'image/png',
          density: '1.5'
        },
        {
          src: 'img/favicons/android-icon-96x96.png',
          sizes: '96x96',
          type: 'image/png',
          density: '2.0'
        },
        {
          src: 'img/favicons/android-icon-144x144.png',
          sizes: '144x144',
          type: 'image/png',
          density: '3.0'
        },
        {
          src: 'img/favicons/android-icon-192x192.png',
          sizes: '192x192',
          type: 'image/png',
          density: '4.0'
        },
        {
          src: 'img/favicons/apple-icon-57x57.png?ver=1.1',
          sizes: '57x57',
          type: 'image/png',
          density: '1.0'
        },
        {
          src: 'img/favicons/apple-icon-72x72.png?ver=1.1',
          sizes: '72x72',
          type: 'image/png',
          density: '1.5'
        },
        {
          src: 'img/favicons/apple-icon-114x114.png?ver=1.1',
          sizes: '114x114',
          type: 'image/png',
          density: '2.5'
        },
        {
          src: 'img/favicons/apple-icon-152x152.png?ver=1.1',
          sizes: '152x152',
          type: 'image/png',
          density: '3.0'
        },
        {
          src: 'img/favicons/apple-icon-180x180.png?ver=1.1',
          sizes: '152x152',
          type: 'image/png',
          density: '3.0'
        },
      ]
    }
  },

  chainWebpack: (config) => {
    config.plugin('html').tap((args) => {
      /* eslint-disable  no-param-reassign */
      args[0].title = 'Size your system • Alfa Laval';
      return args;
    });
  }
};
