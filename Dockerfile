FROM node:lts-alpine3.12 AS build

# Define Variables
ARG ApplicationName="alfalaval"
ARG ApplicationRoot="/opt/app-root/$ApplicationName"
ARG ApplicationGroup="pwebapp"

# Create WORKDIR
RUN mkdir -p "$ApplicationRoot"

# Set WORKDIR
WORKDIR "$ApplicationRoot"

# Copy basic configuration files
COPY package.json vue.config.js babel.config.js "$ApplicationRoot"

# Clean Install
RUN npm install

# Copy SRC
COPY ./src "$ApplicationRoot"/src
COPY ./public "$ApplicationRoot"/public

# Run build prod
RUN npm run build

# ---------------------------------------------------- Production
FROM nginx:stable

# Copy latest build
COPY --from=build /opt /opt

# Setup NGiNX Permissions
RUN chmod g+rwx /var/cache/nginx /var/run /var/log/nginx
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
RUN sed -i.bak 's/^user/#user/' /etc/nginx/nginx.conf

# Expose Container Port
EXPOSE 8080

# Run nginx
CMD ["nginx", "-g", "daemon off;"]