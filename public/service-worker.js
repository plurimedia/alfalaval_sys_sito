importScripts("/precache-manifest.d27f5d040fbb0676256900b6ac9b27c1.js", "https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js");

/* eslint-disable no-restricted-globals */
self.addEventListener('install', (event) => {
  console.log('[Service Worker] Installing Service Worker ...', event);
});

self.addEventListener('activate', (event) => {
  console.log('[Service Worker] Activating Service Worker ...', event);
  return self.clients.claim();
});

self.addEventListener('fetch', (event) => {
  //   console.log('[Service Worker] Fetching something ....', event);
  event.respondWith(fetch(event.request));
});

